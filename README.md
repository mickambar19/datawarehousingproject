# Ruby on Rails - Datawarehousing app (WEKA)

Esta es una aplicacion para la materia de datawarehousing
[*Ruby on Rails:Innoveloper Inc*](http://innoveloper.com/)
by [Datawarehousing Team](http://www.ourteam.com/).

## Licencia

Todo el codigo en [Datawarehousing app](http://dbdatawarehousing.heroku.com/)
esta disponible bajo la licencia MIT License. Mas detalles
[LICENSE.md](LICENSE.md)

## Como inicializar la aplicacion

Para comenzar debes de clonar el repo, y luego instalar las gemas necesarias:

```
$ bundle install --without production
```

Luego, crear el usuario dbpass en MYSQL con su respectiva contrasenia dbpass y enseguida migrar la base de datos:

```
$ rails db:migrate
```

Finalmente, corre la fase de testeo para asegurarnos de que todo ha salido correctamente:

```
$ rails test
```

Si todo sale como lo esperado en la fase de test, la aplicacion estara lista para ser corrida en el servidor local:

```
$ rails server
```

Para mas informacion contactar:
[*Datawarehousing team* Alex](http://innoveloper.com)
