class StaticPagesController < ApplicationController
  def home
  end

  def help
  end

  def about
  end

  def login
  end

  def contact
  end

  def records
    #find the user records

    @user = User.find(params[:id])
    if @user.empty?
      format.html { render :login , notice: 'No existe el usuario. Intenta de nuevo.' }
    else
      @records = @user.records
    end
  end
end
