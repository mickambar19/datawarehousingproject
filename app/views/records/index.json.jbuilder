json.array!(@records) do |record|
  json.extract! record, :id, :user_id, :name, :date
  json.url record_url(record, format: :json)
end
