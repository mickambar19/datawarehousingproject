require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(name: "nombre", email:"ex@hotmail.com", password: "ex123",
                    password: "foobar", password_confirmation: "foobar")
  end

  test "deberia ser valido" do
    assert @user.valid?
  end

  test "nombre deberia estar presente" do
    @user.name = ""
    assert_not @user.valid?
  end

  test "email deberia estar presente" do
    @user.email = "     "
    assert_not @user.valid?
  end

  test "nombre deberia no ser muy largo" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "email no deberia ser muy largo" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "validacion de name deberia de rechazar direcciones invalidas" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} deberia ser valido"
    end
  end

  test "direcciones de email deberian ser unicas" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

  test "direcciones de email deberian guardarse con minusculas" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  test "debe de estar presente la contrasenia" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
end
