require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  def setup
    @base_title = "Datawarehousing Project"
  end

  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "Inicio | #{@base_title}"
  end

  test "should get help" do
    get ayuda_path
    assert_response :success
    assert_select "title", "Ayuda | #{@base_title}"
  end

  test "should get about" do
    get acerca_path
    assert_response :success
    assert_select "title", "Acerca | #{@base_title}"
  end

  test "should get contact" do
    get contacto_path
    assert_response :success
    assert_select "title", "Contacto | #{@base_title}"
  end

end
