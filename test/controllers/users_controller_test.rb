require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "deberia obtener index" do
    get users_url
    assert_response :success
  end

  test "deberia obtener new" do
    get new_user_url
    assert_response :success
  end

  test "deberia crear user" do
    assert_difference('User.count') do
      post users_url, params: { user: { email: @user.email, name: @user.name, password: @user.password } }
    end

    assert_redirected_to user_url(User.last)
  end

  test "deberia mostrar user" do
    get user_url(@user)
    assert_response :success
  end

  test "deberia obtener edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "deberia actualizar user" do
    patch user_url(@user), params: { user: { email: @user.email, name: @user.name, password: @user.password } }
    assert_redirected_to user_url(@user)
  end

  test "deberia eliminar user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end
end
